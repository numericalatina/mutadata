FROM 0xacab.org:4567/radioteca/vikingo:1.2
RUN pip install mutagen
COPY ./radiotk/management/commands/metadata_fix.py /var/www/radioteca/radioteca/radiotk/management/commands/
