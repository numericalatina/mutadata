from django.core.management.base import BaseCommand
from radiotk.models import Audio
import datetime
from mutagen.id3 import ID3, ID3NoHeaderError
from mutagen.easyid3 import EasyID3
import mutagen

class Command(BaseCommand):
    help = """
    Poblar la metadata (ID3) de los archivos subidos a Radioteca, usando la informacion en la base de datos
    """

#    option_list = BaseCommand.option_list + (
#        make_option('--server',
#            action='store',
#            dest='server',
#            default=None,
#            help='WP-Site Host Name.'
#        ),
#    )

    def handle(self, *args, **options):
        # TODO offset...
        agosto=datetime.datetime(2018,8,1) # trabajar con audios de este mes
        audios = Audio.objects.filter(creation_date__gt=agosto)
        for audio in audios:
            self.fix_metadata(audio)

    def fix_metadata(self, audio):
        """ LISTA DE TAGS POSIBLES
        title artist genre album composer conductor copyright comment label organization language last_modified mood website date
        tracknumber track_total mime_type channels bpm encoded_by isrc bit_rate replay_gain
        """
        try:
        # TODO MIME (ogg, wav...)
            metadata = EasyID3(audio.audio.file.name)
            metadata["title"] = audio.name
            metadata["artist"] = audio.user.get_profile().institution_name
            metadata["language"] = audio.get_language_display()
            metadata["date"] = str(audio.production_date)
            metadata["website"] = "https://radioteca.net{}".format(audio.get_absolute_url())
            metadata["length"] = str(int(audio.seconds))
        #    metadata["composer"] = metadata["artist"]
        #    metadata["conductor"] = metadata["artist"]
        #    metadata["label"] = metadata["artist"]
        #    metadata["organization"] = metadata["artist"]
        #    metadata["comment"] = audio.description
        #    metadata["genre"] = "radio"
            if audio.series:
                serie = audio.series
                metadata["album"] = serie.name
                tracknumber, track_total = self.series_tracknumbers(audio)
                metadata["tracknumber"] = tracknumber
                metadata["track_total"] = track_total
        # TODO license
        #    metadata["copyright"] = "Peer Production License"
        # TODO invalid keys (easy?)
        #    metadata["last_modified"] = audio.get_last_change_date
        #    metadata["mime_type"] = audio.file_type
        #    metadata["bit_rate"] = audio.bit_rate
        # TODO (tambien tenemos audio.crc32, audio.sample_rate)
            metadata.save()
            print("OK: {}".format(audio.audio.url))
            
        except ID3NoHeaderError:
            print("No tenia ID3: {}".format(audio.audio.url))
            meta = mutagen.File(audio.audio.file.name, easy=True)
            meta.add_tags()
            meta.save()
            self.fix_metadata(audio) # recurse
            
        except IOError:
            print("Archivo ausente: {}".format(audio.audio.url))

    def series_tracknumbers(self, audio):
        """devuelve el numero del audio en la serie y el total de audios de la serie"""
        serie = audio.series
        siblings = serie.audios()
        track_total = len(siblings)
        counter = 0
        for sibling in siblings:
            counter += 1
            if sibling.pk == audio.pk:
                break
        return counter, track_total
